INTRODUCTION
------------

The denormalized webform filters is adding filter for data directly pulled from a denormalized database table. As data from database are text,
the module provide a select filter for these text fields and use the denormalized webform elements definition to populate the filter options.

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/denormalized_webform_filters

REQUIREMENTS
------------

This module requires the following modules:

 * Views (https://www.drupal.org/project/views)
 * Webform (https://www.drupal.org/project/webform)
 * Denormalizer (https://www.drupal.org/project/denormalizer)

INSTALLATION
 ------------

  * Install as you would normally install a contributed Drupal module. Visit
    https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled and cache cleared, denormalized filter will be available in each view that has a denormalized webform table as a base table.

MAINTAINERS
-----------

 Current maintainers:
   * Daniel Cothran (andileco) - https://www.drupal.org/user/2054544
   * Mamadou D. Diallo (diaodiallo) - https://www.drupal.org/user/3574365

 Supporting organization:
   * John Snow, Inc. (JSI) - https://www.drupal.org/john-snow-inc-jsi