<?php
/**
 * Created by PhpStorm.
 * User: ddiallo
 * Date: 1/10/20
 * Time: 3:19 PM
 */

namespace Drupal\denormalized_webform_filters\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\Display\DisplayPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;

/**
 * Filter denormalized webform database tables.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("dn_webform_filter")
 */
class DenormalizedWebformFilter extends FilterPluginBase {

  protected $baseTable = [];

  protected $finalBaseTable = '';

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->baseTable = $view->getBaseTables();
    unset($this->baseTable['#global']);
    $this->baseTable = array_keys($this->baseTable);
    $this->definition['options callback'] = [$this, 'generateOptions'];
  }

  /**
   * {@inheritdoc}
   */
  protected function canBuildGroup() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function canExpose() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isExposed() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['selected_option'] = ['default' => ''];
    $options['dn_webform_id'] = ['default' => ''];
    $options['dn_field_name'] = ['default' => ''];
    $options['base_table'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state) {

    if ($this->options['exposed']) {

      $form['value'] = !empty($form['value']) ? $form['value'] : '';
      parent::buildExposedForm($form, $form_state);

      $filter_id = $this->getFilterId();
      $webform_id = $this->options['dn_webform_id'];
      $fieldName = $this->options['dn_field_name'];
      $fieldOptions = $this->getFilterOptions($webform_id, $fieldName);

      $form[$filter_id] = [
        '#type' => 'select',
        '#options' => $fieldOptions,
        '#default_value' => 'All',
      ];
      if ($this->options['expose']['multiple']) {
        $form[$filter_id]['#multiple'] = TRUE;
      }
    }

    // $this->valueForm($form, $form_state);
  }


  /**
   * This method returns the ID of the fake field which contains this plugin.
   *
   * It is important to put this ID to the exposed field
   * of this plugin for the following reasons:
   * a) To avoid problems with FilterPluginBase::acceptExposedInput function
   * b) To allow this filter to be printed on
   * twig templates with {{ form.custom_az_filter }}
   *
   * @return string
   *   ID of the field which contains this plugin.
   */
  private function getFilterId() {
    return $this->options['expose']['identifier'];
  }

  protected function valueForm(&$form, FormStateInterface $form_state) {

    if (!$this->options['exposed']) {
      $form['selected_option'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Enter the value to filter with'),
        '#default_value' => isset($this->options['selected_option']) ? $this->options['selected_option'] : NULL,
      ];
    }
  }


  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    foreach ($this->baseTable as $baseTable) {
      $suffix = str_replace('denormalizer_', '', $baseTable);
      $configTable = 'denormalizer.denormalizer_table.' . $suffix;
      $config = \Drupal::service('config.factory')
        ->getEditable($configTable);
      $temConfiguration = $config->get('configuration');
      $tem_webform_id = $temConfiguration['bundle'];
      if ($tem_webform_id != NULL) {
        $this->options['dn_webform_id'] = $tem_webform_id;
        $this->options['base_table'] = $baseTable;
      }
    }
    $fieldOptions = $this->getFieldList($this->options['dn_webform_id']);

    $form['help'] = [
      '#type' => 'item',
      '#title' => $this->t('Denormalized webform id'),
      '#markup' => isset($this->options['dn_webform_id']) ? $this->options['dn_webform_id'] : NULL,
    ];

    $form['dn_field_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the webform field'),
      '#description' => $this->t('The submission will be filtered by this field.'),
      '#options' => $fieldOptions,
      '#default_value' => isset($this->options['dn_field_name']) ? $this->options['dn_field_name'] : NULL,
    ];
  }

  public function acceptExposedInput($input) {
    if (empty($this->options['exposed'])) {
      return TRUE;
    }

    $rc = parent::acceptExposedInput($input);

    return $rc;
  }

  /**
   * Applying query filter. If you turn on views query debugging you should see
   * these clauses applied. If the filter is optional, and nothing is selected,
   * this code will never be called.
   */
  public function query() {
    if ($this->options['base_table'] != '' && $this->options['dn_field_name'] != '') {
      $this->ensureMyTable();

      if (!$this->options['exposed']) {
        $this->query->addWhere("AND", "{$this->options['base_table']}.{$this->options['dn_field_name']}", $this->options['selected_option'], "=");
      }
      else {
        // Exposed value.
        $type = $this->options['expose']['multiple'] ? 'multiple' : 'single';

        if ($type == 'single') {
          // To let single value use the same methods as multiple values.
          $this->value = [$this->value[0] => $this->value[0]];
        }

        $this->queryFilter($this->options['dn_field_name'], $this->value);
      }
    }
  }

  private function queryFilter($fieldName, $lletresSeleccionades) {
    if (!empty($lletresSeleccionades) && ($lletresSeleccionades !== 'All')) {
      $this->query->setWhereGroup('OR', 7);
      foreach ($lletresSeleccionades as $key => $value) {
        $this->query->addWhere(7, "{$this->options['base_table']}.{$fieldName}", $this->securityFilter($value), '=');
      }
    }
  }


  /**
   * Security filter.
   *
   * @param mixed $value
   *   Input.
   *
   * @return mixed
   *   Sanitized value of input.
   */
  private function securityFilter($value) {
    $value = Html::escape($value);
    $value = Xss::filter($value);
    return $value;
  }

  /**
   * It generates all the letters of the alphabet.
   *
   * @return array
   *   Array with all letters indexed by the letters itself.
   */
  private function getFilterOptions($webform_id, $fieldName) {
    $return = [];
    $fieldOptions = $this->getFieldOptions($webform_id, $fieldName);
    $return['All'] = $this->t('ALL');
    foreach ($fieldOptions as $key => $value) {
      $return[$key] = $value;
    }

    return $return;
  }

  public function getFieldOptions($webform_id, $fieldName) {
    if (!is_array($webform_id)) {
      $webform = \Drupal\webform\Entity\Webform::load($webform_id);
    }
    $fieldOptions = [];
    if (isset($webform) && $webform != NULL) {
      $formOptions = $webform->getElementDecoded($fieldName);
      if ($formOptions['#type'] == 'select') {
        $fieldOptions = $formOptions['#options'];
      }
      elseif ($formOptions['#type'] == 'textfield') {
        $results = $this->getFieldData($this->options['base_table'], $fieldName);
        $data = [];
        foreach ($results as $result) {
          array_push($data, $result->$fieldName);
        }
        $data = array_filter($data, 'strlen');
        if (!empty($data) && $data != NULL) {
          foreach ($data as $key => $value) {
            $fieldOptions[$value] = $value;
          }
        }

      }
    }

    return $fieldOptions;
  }

  private function getFieldData($baseTable, $fieldName) {

    $query = \Drupal::database()->select($baseTable, 'td');
    $query->addField('td', $fieldName);
    $term = $query->execute();
    $tname = $term->fetchAll();

    return $tname;
  }


  public function getFieldList($webform_id) {
    $webform = \Drupal\webform\Entity\Webform::load($webform_id);
    $fields = [];
    $elements = $webform->getElementsDecoded();
    foreach ($elements as $element_id => $element) {
      if ($element['#type'] == 'select' || $element['#type'] == 'textfield') {
        $fields[$element_id] = $element['#title'];
      }
    }

    return $fields;
  }

}